using Microsoft.AspNetCore.Identity;
using UserManagement;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddSingleton(UserManager.GetInstance());
builder.Services.AddCors(options => options.AddPolicy("AlloweAcess_To_Api",
    policy => policy
    .AllowAnyOrigin()
    .AllowAnyMethod()
    .AllowAnyHeader()
));

var app = builder.Build();
app.UseCors("AlloweAcess_To_Api");
app.UseRouting();

// Configure the HTTP request pipeline.
app.UseCors(builder => builder
    .AllowAnyOrigin()
    .AllowAnyMethod()
    .AllowAnyHeader());
app.UseHttpsRedirection();
app.UseAuthorization();

app.MapControllers();

app.Run();
