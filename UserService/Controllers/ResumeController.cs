using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UserManagement;

namespace UserService.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    [ApiController]
    [Route("[controller]")]
    public class ResumeController : ControllerBase
    {
        private readonly ILogger<ResumeController> _logger;
        private readonly UserManager _userManager;
        private const string RESUME_DIR = @"C:\MatchTech\Resume";

        public ResumeController(ILogger<ResumeController> logger, UserManager userManager)
        {
            _logger = logger;
            _userManager = userManager;
        }

        [HttpPost("{authToken}")]
        public async Task<IActionResult> UploadResume([FromForm(Name = "pdfFile")] IFormFile file, string authToken)
        {
            // Check if a file was actually uploaded
            if (file == null || file.Length == 0)
                return BadRequest("No file was uploaded.");

            // Check if the file is a PDF
            if (Path.GetExtension(file.FileName) != ".pdf")
                return BadRequest("Only PDF files are allowed.");

            string userId = await this._userManager.GetMailById(authToken);
            if (string.IsNullOrEmpty(userId))
            {
                return Unauthorized("The auth token illegal");
            }
            // Save the file to disk
            if (!Directory.Exists(RESUME_DIR))
            {
                // If the directory doesn't exist, create it
                Directory.CreateDirectory(RESUME_DIR);
            }
            var filePath = Path.Combine(RESUME_DIR, $"{userId}.pdf");
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }



            // Return a success message
            return Ok($"File {file.FileName} has been uploaded.");
        }

        [HttpDelete("{authToken}")]
        public async Task<IActionResult> DeleteResume([FromQuery] string authToken)
        {
            string userId = await this._userManager.GetMailById(authToken);
            if (string.IsNullOrEmpty(userId))
            {
                return Unauthorized("The auth token illegal");
            }
            var filePath = Path.Combine(RESUME_DIR, $"{userId}.pdf");
            FileInfo fileInfo = new FileInfo(filePath);
            if (!fileInfo.Exists)
            {
                return NotFound("There is no file to delete.");
            }

            // Return a success message
            return Ok($"File has been deleted.");
        }
    }
}
