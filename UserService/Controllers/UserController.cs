using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UserManagement;
using UserManagement.Models;
using UserService.Model;

namespace UserService.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly ILogger<UsersController> _logger;
        private readonly UserManager _userManager;

        public UsersController(ILogger<UsersController> logger, UserManager userManager)
        {
            _logger = logger;
            _userManager = userManager;
        }

        /// <summary>
        /// checks if the server side alive
        /// </summary>
        /// <returns>is server side alive</returns>
        [Route("isAlive")]
        [HttpGet]
        public bool IsAlive()
        {
            return true;
        }

        [Route("login")]
        [HttpPost]
        public async Task<ActionResult<string>> Login([FromBody] LoginRequestDTO loginRequest)
        {
            // Validate the request
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            string authToken = await _userManager.TryLogin(loginRequest.Email, loginRequest.Password);
            if (String.IsNullOrEmpty(authToken))
            {
                return Unauthorized();
            }
            // Return the auth token to the client
            return Ok(new { authToken });
        }


        [Route("register")]
        [HttpPost]
        public async Task<ActionResult<string>> Register([FromBody] RegisterRequestDTO registerRequest)
        {
            // Validate the request
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            string authToken = await _userManager.TryRegister(registerRequest.FullName, registerRequest.Email, registerRequest.Password, registerRequest.IsRecuiter);
            if (String.IsNullOrEmpty(authToken))
            {
                return Unauthorized();
            }
            // Return the auth token to the client
            return Ok(new { authToken });
        }


        [Route("updateSkills/{authToken}")]
        [HttpPut]
        public async Task<ActionResult> UpdateSkills([FromBody] List<string> skills, string authToken)
        {
            // Validate the request
            if (skills == null)
            {
                return BadRequest();
            }
            bool updateResult = await _userManager.UpdateSkillsList(authToken, skills);
            if (updateResult)
            {
                return Ok();
            }
            return BadRequest();
        }

        [Route("updateExprianceYears/{authToken}")]
        [HttpPut]
        public async Task<ActionResult> UpdateExprianceYears([FromBody] int exprianceYears, string authToken)
        {
            bool updateResult = await _userManager.UpdateExprianceYears(authToken, exprianceYears);
            if (updateResult)
            {
                return Ok();
            }
            return BadRequest();
        }

        [Route("updateSalary/{authToken}")]
        [HttpPut]
        public async Task<ActionResult> UpdateUserSalary([FromBody] SalaryDTO salaryDto, string authToken)
        {
            if (!ModelState.IsValid || salaryDto.MinSalary >= salaryDto.MaxSalary)
            {
                return BadRequest(ModelState);
            }
            bool updateResult = await _userManager.UpdateSalary(salaryDto.MinSalary, salaryDto.MaxSalary, authToken);
            if (updateResult)
            {
                return Ok();
            }
            return BadRequest();
        }

        [Route("getSkills/{authToken}")]
        [HttpGet]
        public async Task<ActionResult> GetSkills(string authToken)
        {
            var userSkills = await _userManager.GetCandidateSkills(authToken);
            return Ok(userSkills);
        }

        [Route("getSalaryRange/{authToken}")]
        [HttpGet]
        public async Task<ActionResult<SalaryDTO>> GetSalaryRange(string authToken)
        {
            var user = (CandidateModel)await _userManager.GetUserByAuth(authToken);
            return Ok(new SalaryDTO()
            {
                MinSalary = user.MinSalary,
                MaxSalary = user.MaxSalary,
            });
        }

        [Route("getExprianceYears/{authToken}")]
        [HttpGet]
        public async Task<ActionResult> GetExprianceYears(string authToken)
        {
            var exprianceYears = ((CandidateModel)(await _userManager.GetUserByAuth(authToken))).ExprianceYears;
            return Ok(exprianceYears);
        }

        [Route("isRecuiter/{authToken}")]
        [HttpGet]
        public async Task<ActionResult> IsRecuiter(string authToken)
        {
            return Ok(await _userManager.IsRecuiter(authToken));
        }
    }
}