﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserService.Model
{
    public class UserDTO
    {
        public string Username { get; set; }
        public string MailAddress { get; set; }
        public int MinSalary { get; set; }
        public int MaxSalary { get; set; }
    }
}
