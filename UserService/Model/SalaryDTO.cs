﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserService.Model
{
    public class SalaryDTO
    {
        public int MinSalary { get; set; }
        public int MaxSalary { get; set; }
    }
}
