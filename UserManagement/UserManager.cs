using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using UserManagement.Models;

namespace UserManagement
{
    public class UserManager
    {
        private static UserManager _instance;
        private readonly MongoManager _mongoManager;
        private readonly IMongoCollection<UserModel> _usersCollection;
        private readonly IMongoCollection<RecruiterModel> _recruitersCollection;
        private const string DB_NAME = "MatchTech";
        private const string COLLECTION_NAME = "Users";

        public static UserManager GetInstance()
        {
            return _instance ??= new UserManager();
        }

        public UserManager()
        {
            _mongoManager = new MongoManager(DB_NAME);
            _usersCollection = _mongoManager.MongoDatabase.GetCollection<UserModel>(COLLECTION_NAME);
            _recruitersCollection = _mongoManager.MongoDatabase.GetCollection<RecruiterModel>(COLLECTION_NAME);
        }

        public async Task<string> TryLogin(string email, string password)
        {
            // Look up the user in the database
            var filter = Builders<UserModel>.Filter.Eq(user => user.Email, email);
            var user = await _usersCollection.Find(filter).SingleOrDefaultAsync();
            if (user == null)
            {
                return String.Empty;
            }

            // Check the password
            string hashedPassword = user.Password;
            if (!VerifyPassword(password, hashedPassword))
            {
                return String.Empty;
            }

            // Generate a new authentication token
            string authToken = Guid.NewGuid().ToString();

            // Update the user's record to include the new auth token
            var update = Builders<UserModel>.Update.Set(user => user.AuthToken, authToken);
            await _usersCollection.UpdateOneAsync(filter, update);

            return authToken;
        }

        public async Task<string> TryRegister(string fullName, string email, string password, bool isRecuiter)
        {
            // Look up the user in the database
            var filter = Builders<UserModel>.Filter.Eq(user => user.Email, email);
            var user = await _usersCollection.Find(filter).SingleOrDefaultAsync();
            if (user != null)
            {
                return String.Empty;
            }

            string hashedPassword = HashPassword(password);
            // Generate a new authentication token
            string authToken = Guid.NewGuid().ToString();
            UserModel userDocument = (isRecuiter) ? new RecruiterModel() {
                    Email = email,
                    Password = hashedPassword,
                    FullName = fullName,
                    AuthToken = authToken }: new CandidateModel() {
                    Email = email,
                    Password = hashedPassword,
                    FullName = fullName,
                    AuthToken = authToken,
                    Skills = new List<string>()};
            await _usersCollection.InsertOneAsync(userDocument);
            return authToken;
        }

        private static string HashPassword(string password)
        {
            // Convert the password and hashed password to byte arrays
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);

            // Create a new instance of the hash algorithm
            HashAlgorithm hashAlgorithm = new SHA256CryptoServiceProvider();


            // Calculate the hash of the provided password
            byte[] passwordHash = hashAlgorithm.ComputeHash(passwordBytes);

            return Convert.ToBase64String(passwordHash);
        }

        public async Task<bool> ValidateAuthToken(string authToken)
        {
            // Look up the user in the database
            var usersCollection = _mongoManager.MongoDatabase.GetCollection<UserModel>(COLLECTION_NAME);
            var filter = Builders<UserModel>.Filter.Eq(user => user.AuthToken, authToken);
            var user = await usersCollection.Find(filter).SingleOrDefaultAsync();
            return user != null;
        }

        private bool VerifyPassword(string password, string hashedPassword)
        {
            // Convert the password and hashed password to byte arrays
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            byte[] hashedPasswordBytes = Convert.FromBase64String(hashedPassword);

            // Create a new instance of the hash algorithm
            HashAlgorithm hashAlgorithm = new SHA256CryptoServiceProvider();


            // Calculate the hash of the provided password
            byte[] passwordHash = hashAlgorithm.ComputeHash(passwordBytes);

            // Compare the hashed password to the provided password hash
            return hashedPasswordBytes.SequenceEqual(passwordHash);
        }

        public async Task<string> GetMailById(string authToken)
        {
            // Look up the user in the database
            var usersCollection = _mongoManager.MongoDatabase.GetCollection<UserModel>(COLLECTION_NAME);
            var filter = Builders<UserModel>.Filter.Eq(user => user.AuthToken, authToken);
            var user = await usersCollection.Find(filter).SingleOrDefaultAsync();
            if (user == null)
            {
                return string.Empty;
            }
            return user.Id;
        }

        public async Task<UserModel> GetUserByAuth(string authToken)
        {
            // Look up the user in the database
            var usersCollection = _mongoManager.MongoDatabase.GetCollection<UserModel>(COLLECTION_NAME);
            var filter = Builders<UserModel>.Filter.Eq(user => user.AuthToken, authToken);
            return await usersCollection.Find(filter).SingleOrDefaultAsync();
        }

        public async Task<bool> IsRecuiter(string authToken)
        {
            var filter = Builders<UserModel>.Filter.Eq(user => user.AuthToken, authToken);
            var user = await _usersCollection.Find(filter).SingleOrDefaultAsync();
            if (user == null) return false;
            return user.IsRecuiter;
        }


        public async Task<IEnumerable<string>> GetCandidateSkills(string authToken)
        {
            var candidate = (CandidateModel)await _usersCollection.Find(user => user.AuthToken == authToken && !user.IsRecuiter).SingleOrDefaultAsync();
            if (candidate != null)
            {
                return candidate.Skills;
            }
            return new List<string>();
        }

        public async Task<bool> UpdateSkillsList(string authToken, List<string> updatedSkills)
        {
            var filter = Builders<UserModel>.Filter.Eq(user => user.AuthToken, authToken);
            var update = Builders<UserModel>.Update.Set(user => ((CandidateModel)user).Skills, updatedSkills);
            var user = await _usersCollection.FindOneAndUpdateAsync<CandidateModel>(filter, update);
            if (user != null)
            {
                return true;
            }
            return false;
        }

        public async Task<bool> UpdateExprianceYears(string authToken, int exprianceYears)
        {
            var filter = Builders<UserModel>.Filter.Eq(user => user.AuthToken, authToken);
            var update = Builders<UserModel>.Update.Set(user => ((CandidateModel)user).ExprianceYears, exprianceYears);
            var user = await _usersCollection.FindOneAndUpdateAsync(filter, update);
            if (user != null)
            {
                return true;
            }
            return false;
        }

        public async Task<bool> UpdateSalary(int minSalary, int maxSalary, string authToken)
        {
            var filter = Builders<UserModel>.Filter.Eq(user => user.AuthToken, authToken);
            var update = Builders<UserModel>.Update
                                                   .Set(user => ((CandidateModel)user).MinSalary, minSalary)
                                                   .Set(user => ((CandidateModel)user).MaxSalary, maxSalary);
            var user = await _usersCollection.FindOneAndUpdateAsync(filter, update);
            if (user != null)
            {
                return true;
            }
            return false;
        }

        public async Task<List<UserModel>> GetAllUsers()
        {
            return await _usersCollection.Find(_ => true).ToListAsync();
        }

        public async Task<UserModel> GetUserByAuthToken(string authToken)
        {
            // Look up the user in the database
            var usersCollection = _mongoManager.MongoDatabase.GetCollection<UserModel>(COLLECTION_NAME);
            var filter = Builders<UserModel>.Filter.Eq(user => user.AuthToken, authToken);
            var user = await usersCollection.Find(filter).SingleOrDefaultAsync();
            if (user == null)
            {
                return null;
            }
            return user;
        }
    }
}
